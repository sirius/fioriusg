sap.ui.define([
	"sap/ui/base/Object"
], function(BaseObject) {
   "use strict";
   
	/**
	 * String Utilities
	 * 
	 * @class
	 * @public
	 * @name org.sapux.fioriusg.util.StringUtil
	 */
	var StringUtil = BaseObject.extend("org.sapux.fioriusg.util.StringUtil", {
   	
   		/**
   		 * @public
   		 */
   		constructor: function() {

   		}
   });
   
   /**
   	* Convert Javascript date to ABAP DATS (YYYYMMDD) string
   	*/
   StringUtil.dateToABAP = function(dDate) {
   		var nMonth = dDate.getMonth() + 1;
   		var sMonth = nMonth < 10? "0"+nMonth : ""+nMonth;
   		var nDate = dDate.getDate();
   		var sDate = nDate < 10? "0"+nDate : "" + nDate;
   		return dDate.getFullYear() + sMonth + sDate;
   		
   };
   
   return StringUtil;
});