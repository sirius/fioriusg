sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"org/sapux/fioriusg/util/StringUtil"
], function(JSONModel, Device, StringUtil) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		
		createLocalModel: function() {
			var oModel = new JSONModel();
			oModel.setData({
				range: [
					{key:"last-30-days", text:"Last 30 Days"},
					{key:"last-3-months", text:"Last 3 Months"},
					{key:"last-6-months", text:"Last 6 Months"},
					{key:"this-year", text:"This Year"},
					{key:"custom", text:"Custom Range"}
				],
				selectedRange: 'last-30-days',
				calendar: {
					date1: new Date(2019, 4, 1),
					date2: new Date(2019, 4, 31),
				},
				granularity: [
					{key:"d", text:"Daily"},
					{key:"w", text:"Weekly"},
					{key:"m", text:"Monthly"}
				],
				selectedGranularity: "d",
				dateSelection: "20190501-20190531-d"
			});
			this.updateDateSelection(oModel,"range"); // init calendar and granularity
			return oModel;
		},
		
		/**
		* Update date selection model base on source,
		* @param sSource: source of change for instance: range, calendar, granularity
		* @param sDate (optional): selected date for instance '05/01' from charts
		*/
		updateDateSelection: function(oModel, sSource, sDate) {
			var oData = oModel.getData();
			var d1 = null, d2 = null, g = "d", now = new Date(); 
			if (sSource === "range" ) {
				var sNewRange = oData.selectedRange;
				if ( sNewRange === "last-30-days" ) {
					d2 = now;
					d1 = new Date( d2.getTime() - 30 * 86400000 );
					g = "d";
				} else if ( sNewRange.indexOf("last") === 0 ) {
					var nMonth = Number(sNewRange.split("-")[1]);
					d1 = new Date(now.getFullYear(), now.getMonth() - nMonth + 1, 1);
					d2 = new Date(new Date(now.getFullYear(), now.getMonth() + 1, 1).getTime() - 1000); // this month last day 23:59:59
					g = "d";
				} else if ( sNewRange == "this-year" ) {
					d1 = new Date(now.getFullYear(), 0, 1);
					d2 = new Date(now.getFullYear(), 11, 31);
					g = "m";
				} else {
					// custom
					return; // nothing need to update for custom range
				}
				oData.calendar.date1 = d1;
				oData.calendar.date2 = d2;
				oData.selectedGranularity = g;
				oData.dateSelection = StringUtil.dateToABAP(d1) + "-" + StringUtil.dateToABAP(d2) + "-" + g;
			} else if (sSource === "calendar") {
				oData.selectedRange = "custom";
				oData.dateSelection = StringUtil.dateToABAP(oData.calendar.date1) + "-" + StringUtil.dateToABAP(oData.calendar.date2)  + "-" +oData.selectedGranularity;
			} else if (sSource === "granularity") {
				oData.dateSelection = StringUtil.dateToABAP(oData.calendar.date1) + "-" + StringUtil.dateToABAP(oData.calendar.date2)  + "-" +oData.selectedGranularity;
			} else if (sSource === "chart" ) {
				oData.selectedRange = "custom";
				d1 = oData.calendar.date1;
				var aTokens = sDate.split("/");
				var d3 = new Date( d1.getFullYear(),  Number(aTokens[0]) - 1, Number(aTokens[1]) );
				if ( d3.getTime() < d1.getTime() ) {
					// advance one year, FIXME: date selection across multiple years
					d3 = new Date( d3.getFullYear() + 1, d3.getMonth(), d3.getDate() ); 
				}
				var d4 = d3;
				if (oData.selectedGranularity == "m") {
					d4 = new Date( new Date(d3.getFullYear(), d3.getMonth() + 1, 1).getTime() - 1000 );
					oData.selectedGranularity = "d";
				}
				oData.calendar.date1 = d3;
				oData.calendar.date2 = d4;
				oData.dateSelection = StringUtil.dateToABAP(d3) + "-" + StringUtil.dateToABAP(d4) + "-" + oData.selectedGranularity;
			}
			oModel.setData(oData); 
		}
	};
});