sap.ui.define([
	"org/sapux/fioriusg/controller/BaseController",
	"org/sapux/fioriusg/util/CSVGenerator",
	"org/sapux/fioriusg/model/models"
], function(BaseController,CSVGenerator,models) {
	"use strict";

	return BaseController.extend("org.sapux.fioriusg.controller.L1", {
		
        onInit : function (oEvent) {
        	var oDayChart = this.getView().byId("dateChart");
            oDayChart.setVizProperties({
            	categoryAxis: {
                    title: {
                        visible: false
                    }
                },
                title: {
                    visible: false
                }
            });
            var oDeviceChart = this.getView().byId("deviceChart");
            oDeviceChart.setVizProperties({
                title: {
                    visible: false
                }
            });
        },
        
        onAfterRendering: function(oEvent) {
        	 this.getModel().metadataLoaded().then(function() {
            	this.handleDateChange("range");	
        	 }.bind(this));
        },
        
        // Take care of the navigation through the hierarchy when the
		// user selects a table row
		handleSelectionChange: function (oEvent) {
			// Determine where we are right now
			var sPath = oEvent.getParameter("listItem").getBindingContext().getPath();
			var sKey = sPath.match(/'([^']+)'/)[1];
	
			// navigate to L2 with key
			this.getRouter().navTo("L2",{K:sKey});
		},
		
		onRefresh: function(oEvent) {
			this.getView().getModel().refresh();
		},
		
		onDownload: function(oEvent) {
			this.getView().getModel().read("/UsageRawSet", {
				success: function(oData) {
					var oCSVGenerator = new CSVGenerator();
					oCSVGenerator.generate(oData.results);
					oCSVGenerator.download("FioriUsage");
				}
			});
		},
		
		
		/** handle date change via specified source and (optional) selected date string (from chart) */
		handleDateChange: function(sSource, sDate) {
			var oModel = this.getModel("local");
			models.updateDateSelection(oModel,sSource, sDate);
			var sKeys = "r-"+oModel.getData().dateSelection;
			var oContext2 = new sap.ui.model.Context(this.getModel(), "/UsageSet('"+sKeys+"')");
			this.getView().setBindingContext(oContext2);
		},
		
		
		/** user selected a date (or month) in vis chart */
		handleSelectDate: function(oEvent) {
			if ( !(oEvent && oEvent.getParameters() && oEvent.getParameters().data && oEvent.getParameters().data.length) ) {
				return; // no date selected
			}	
			var sDate = oEvent.getParameters().data[0].data['Date'];
			this.handleDateChange("chart",sDate);      
			// var oModel = this.getModel("local");
			// models.updateDateSelection(oModel,"chart", sDate);
			// var sKeys = "r-"+oModel.getData().dateSelection;
			// // navigate to L2 with key
			// this.getRouter().navTo("L2",{K:sKeys});
		},
		
		/** user selected a range from dropdown */
		handleRangeChange: function(oEvent) {
			this.handleDateChange("range",null);
		},
		
		/** user selected date in calendar */
		handleCalendarChange: function (oEvent) {
			this.handleDateChange("calendar",null);
		},
		
		/** user changed date granularity */
		handleGranularityChange: function(oEvent) {
			this.handleDateChange("granularity",null);
		}
     
     
	});
});