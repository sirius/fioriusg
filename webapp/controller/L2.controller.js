sap.ui.define([
	"org/sapux/fioriusg/controller/BaseController",
	"org/sapux/fioriusg/util/CSVGenerator",
	"org/sapux/fioriusg/model/models"
], function(BaseController,CSVGenerator,models) {
	"use strict";

	return BaseController.extend("org.sapux.fioriusg.controller.L2", {
		
		sKeys : "",
		
        onInit : function (oEvent) {
        	
        	var oDayChart = this.getView().byId("dateChart");
            oDayChart.setVizProperties({
            	categoryAxis: {
                    title: {
                        visible: false
                    }
                },
                title: {
                    visible: false
                }
            });
            var oDeviceChart = this.getView().byId("deviceChart");
            oDeviceChart.setVizProperties({
                title: {
                    visible: false
                }
            });
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("L2").attachPatternMatched(this.onRouteMatched, this);
        },
        
        onPressL1: function(oEvent) {
        	this.getRouter().navTo("L1");
        },
        
        onPressLink: function(oEvent) {
        	this.updateSelection(oEvent.getSource().getId());
        },
        
        onRouteMatched: function (oEvent) {
        	var sKey = oEvent.getParameter("arguments").K;
        	this.updateSelection(sKey);
		},
		
		updateSelection: function(sKeys) {
			if (!sKeys)
				return;
			sKeys = decodeURI(sKeys);
			this.sKeys = sKeys;
			this.getView().byId("userPanel").setVisible(true);
			this.getView().byId("appPanel").setVisible(true);
			this.getView().byId("devicePanel").setVisible(true);
			this.getView().byId("datePanel").setVisible(true);
			var aKeys = sKeys.split(/\|/);
			var sLinkLabel = "";
			var sLinkKey = "";
			var oLinks = this.getView().byId("links");
			oLinks.destroyLinks();
			oLinks.addLink(new sap.m.Link({text:"Fiori Usage Report",press:jQuery.proxy(this.onPressL1,this)}));
			for ( var i = 0; i < aKeys.length; i++ ) {
				var sKey = aKeys[i];
				var sValue = sKey.substring(2);
				sLinkLabel = "";
				if (sKey.indexOf("u-") === 0) {
					this.getView().byId("userPanel").setVisible(false);
					sLinkLabel = "User: " + sValue;
				} else if (sKey.indexOf("a-") === 0) {
					this.getView().byId("appPanel").setVisible(false);
					sLinkLabel = "App: " + sValue;
				} else if (sKey.indexOf("d-") === 0) {
					this.getView().byId("datePanel").setVisible(false);
					sLinkLabel = "Date: " + sValue;
				} else if (sKey.indexOf("t-") === 0) {
					this.getView().byId("devicePanel").setVisible(false);
					sLinkLabel = "Device: " + sValue;
				}
				// else if (sKey.indexOf("r-") === 0) {
				// 	var aTokens = sKey.split("-");
				// 	sLinkLabel = "Date: " + aTokens[1] + " ~ " + aTokens[2];
				// }
				if ( !sLinkLabel )
					continue;
				if ( !sLinkKey )
					sLinkKey = sKey;
				else
					sLinkKey = sLinkKey + "|" + sKey;
				if ( i < aKeys.length - 1) {
					oLinks.addLink(new sap.m.Link({id:sLinkKey, text:sLinkLabel, press:jQuery.proxy(this.onPressLink,this)}));
				}
			}
			oLinks.setCurrentLocationText(sLinkLabel);
			
			// extract date range key
			var oModel = this.getModel("local");
			if (oModel && oModel.getData() && sKeys.indexOf("r-") == -1) {
				var sRangeKey = "r-"+oModel.getData().dateSelection;
				sKeys = sRangeKey + "|" + sKeys;
			}
			
			// update binding context
			var oContext = new sap.ui.model.Context(this.getModel(), "/UsageSet('"+sKeys+"')");
			this.getView().setBindingContext(oContext);
		},
		
		 // Take care of the navigation through the hierarchy when the
		// user selects a table row
		handleSelectionChange: function (oEvent) {
			// Determine where we are right now
			var sPath = oEvent.getParameter("listItem").getBindingContext().getPath();
			var sKey = sPath.match(/'([^']+)'/)[1];
	
			// navigate to L2 with key
			if (!sKey)
				this.getRouter().navTo("L1");
			else
				this.updateSelection(sKey);
		},
		
		formatDate : function(v) {
		     jQuery.sap.require("sap.ui.core.format.DateFormat");
		     var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({pattern: "MM/dd/YYYY"});
		     return oDateFormat.format(new Date(v));
		},
		
		downloadData: function(oEvent) {
			var thatKey = this.sKeys;
			this.getView().getModel().read("/UsageSet('"+thatKey+"')/Recent", {
				success: function(oData) {
					var oCSVGenerator = new CSVGenerator();
					oCSVGenerator.generate(oData.results);
					oCSVGenerator.download("FioriUsage-"+thatKey);
				}
			});
		},
		
		/** handle date change via specified source and (optional) selected date string (from chart) */
		handleDateChange: function(sSource, sDate) {
			var oModel = this.getModel("local");
			models.updateDateSelection(oModel,sSource, sDate);
			var sKeys = "r-"+oModel.getData().dateSelection;
			var oContext2 = new sap.ui.model.Context(this.getModel(), "/UsageSet('"+sKeys+"')");
			this.getView().setBindingContext(oContext2);
		},
		
		/** user selected a date (or month) in vis chart */
		handleSelectDate: function(oEvent) {
			if ( !(oEvent && oEvent.getParameters() && oEvent.getParameters().data && oEvent.getParameters().data.length) ) {
				return; // no date selected
			}	
			var sDate = oEvent.getParameters().data[0].data['Date'];
			this.handleDateChange("chart",sDate);
		},
		
		/** user selected a range from dropdown */
		handleRangeChange: function(oEvent) {
			this.handleDateChange("range",null);
		},
		
		/** user selected date in calendar */
		handleCalendarChange: function (oEvent) {
			this.handleDateChange("calendar",null);
		},
		
		/** user changed date granularity */
		handleGranularityChange: function(oEvent) {
			this.handleDateChange("granularity",null);
		}
     
	});
});