# README #

This Fiori Usage Report shows statistics of Fiori access incluing visitors per day, per device type, per app and per user. 

### What is this repository for? ###

* ./webapp/ - SAPUI5 application
* ./abap/ - Backend GW objects

### How do I get set up? ###

- Import ABAP classes and function modules and DDIC objects in ./abap/ folder
- Upload webapp to ABAP repository
- Run function module zdbs_fiori_usage_import

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Reference ### 

For decoding HEX JSON string (via FMCALL) use below tool:
http://www.online-toolz.com/tools/text-unicode-entities-convertor.php

For formatting / beautifying JSON use this tool:
https://jsonformatter.org/

Test Application Online:
https://sapux.org/demo/fioriusg
