function zdbs_fiori_usage_import.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(START_DATE) TYPE  DATS OPTIONAL
*"  EXPORTING
*"     REFERENCE(RET_MESSAGE) TYPE  STRING
*"----------------------------------------------------------------------
  data: lt_ui2_item type table of /ui2/item, " existing /ui2/table
        ls_ui2_item like line of lt_ui2_item,
        lt_usage    type table of zdbs_fiori_usage, " destination fiori_usage table to be written to
        ls_usage    like line of lt_usage.

  " step-1: select /ui2/item into local table
  select client, user_id, value
    from /ui2/item
    where cont_id = 'sap.ushell.services.UserRecents'
    into corresponding fields of table @lt_ui2_item.

  " step-2: parse /ui2/item into ls_usage format
  types: begin of ty_oitem,
           title   type string,
           apptype type string,
           url     type string,
           appid   type string,
         end of ty_oitem,

         begin of ty_usage,
           oitem       type ty_oitem,
           ausagearray type standard table of integer3 with non-unique default key,
           icount      type integer3,
           desktop     type abap_bool,
         end of ty_usage,

         begin of ty_json,
           recentday        type string,
           recentusagearray type standard table of ty_usage with non-unique default key,
         end of ty_json.

  data : ls_json      type ty_json,
         lv_year(4)   type c,
         lv_month(2)  type c,
         lv_day(2)    type c,
         lv_date      type d,
         lv_len       type i,
         lv_index     type i,
         lv_day_count type i,
         lv_str type string.

  loop at lt_ui2_item into ls_ui2_item.

    " initialize the line
    clear ls_usage.
    ls_usage-client = ls_ui2_item-client.
    ls_usage-user_id = ls_ui2_item-user_id.

    "deserialize the json to usage arrage
    /ui2/cl_json=>deserialize(
      exporting
        json = ls_ui2_item-value
        pretty_name = /ui2/cl_json=>pretty_mode-camel_case
      changing
        data = ls_json
    ).

    " convert recentDay from YYYY/MM/DD into YYYYMMDD
    split ls_json-recentday at '/' into lv_year lv_month lv_day.
    lv_len = strlen( lv_month ).
    if lv_len = 1.
      concatenate '0' lv_month into lv_month.
    endif.
    lv_len = strlen( lv_day ).
    if lv_len = 1.
      concatenate '0' lv_day into lv_day.
    endif.
    concatenate lv_year lv_month lv_day into lv_date.
    if start_date is not initial and lv_date < start_date. " the user didn't visit FLP on or after the start date
      continue. " continue ls_ui2_item loop.
    endif.

    " convert usage data per user into a flat table
    loop at ls_json-recentusagearray into data(ls_recent).
      "TODO - MAKE SURE THERE ARE NO TWO ENTRIES FOR SAME FIORI APP ID FOR THE SAME USER IN RECENTUSAGEARRAY
      ls_usage-fiori_app_title = ls_recent-oitem-title.
      ls_usage-fiori_app_id = ls_recent-oitem-appid.
      ls_usage-fiori_app_url = ls_recent-oitem-url.
      if ls_recent-desktop eq abap_true.
        ls_usage-device_type = 'D'. " DESKTOP
      else.
        ls_usage-device_type = 'M'. " MOBILE
      endif.
      if ls_recent-oitem-apptype = 'Application'.
        ls_usage-app_type = 'A'.
      elseif ls_recent-oitem-apptype = 'Search'.
        ls_usage-app_type = 'S'.
      elseif ls_recent-oitem-apptype = 'URL'.
        ls_usage-app_type = 'U'.
      endif.

      " now its the tricky part: slices aUsageArray into multiple days counter
      describe table ls_recent-ausagearray lines lv_len.
      lv_index = 1.
      loop at ls_recent-ausagearray into lv_day_count.
        ls_usage-visit_date = lv_date - lv_len + lv_index.
        ls_usage-visit_count = lv_day_count.
        " append day token to result
        if lv_day_count > 0.
          append ls_usage to lt_usage. "only store postive counters
        endif.
        lv_index = lv_index + 1.
      endloop. " end of aUsageArray loop
    endloop. " end of recentUsageArray loop
  endloop. " end of /ui2/item loop, finished pasing

  " count lines of records to be updated
  describe table lt_usage lines lv_len.
  lv_str = lv_len.
  condense lv_str.

  " step-3: update zdbs_fiori_usage table
  modify zdbs_fiori_usage from table lt_usage.

  " Step-4: completed update, output some message
  concatenate 'Updated' lv_str 'line(s) of Fiori Usage data' into ret_message separated by space.
  if start_date is not initial.
    concatenate start_date+4(2) '/'start_date+6(2) '/' start_date(4) into lv_str. "format date into MM/DD/YYYY
    concatenate ret_message 'dated from' lv_str into ret_message separated by space.
  endif.

endfunction.