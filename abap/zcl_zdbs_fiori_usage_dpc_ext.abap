  method usageset_get_entityset.
    data: lt_usage      type table of zdbs_fiori_usage_line,
          lv_start_date type dats,
          lv_end_date   type dats,
          lv_g          type string, " granularity
          ls_path       like line of it_navigation_path,
          ls_key        like line of it_key_tab,
          lv_keys       type string,
          lt_keys       type table of string,
          lv_key        type string,
          lv_where      type string,
          lv_query      type string,
          lv_str        type string.

    field-symbols: <fs_usage> like line of lt_usage.

    " retrieve query params
    lv_end_date = sy-datum.
    lv_start_date = lv_end_date - 30. "default date range: last 30 days
    lv_g = 'd'. "daily

    loop at it_navigation_path into ls_path.
      lv_query = ls_path-nav_prop. " last navigation property is the query we want to execute
    endloop.
    loop at it_key_tab into ls_key.
      lv_keys = ls_key-value.
      if lv_keys is not initial.
        split lv_keys at '|' into table lt_keys.
        loop at lt_keys into lv_key.
          if lv_key(2) = 'u-'. "filter by user.
            concatenate lv_where ' user_id = ''' lv_key+2 ''' and ' into lv_where.
          elseif lv_key(2) = 'a-'. "filter by app.
            concatenate lv_where ' fiori_app_id = ''' lv_key+2 ''' and ' into lv_where.
          elseif lv_key(2) = 'd-'. "filter by date
            concatenate lv_where ' visit_date = ''' lv_key+2 ''' and ' into lv_where.
          elseif lv_key(2) = 't-'. "filter by device_type.
            concatenate lv_where ' device_type = ''' lv_key+2(1) ''' and ' into lv_where.
          elseif lv_key(2) = 'r-'. "date range
            lv_start_date = lv_key+2(8).
            lv_end_date = lv_key+11(8).
            lv_g = lv_key+20(1).
          endif.
        endloop.
      endif.
    endloop.

    concatenate 'visit_date >=''' lv_start_date ''' and visit_date <= ''' lv_end_date  '''' into lv_where.

    " query
    if lv_query = 'ByDate'. " query by date
      if lv_g = 'm'. " group by month
        select substring( visit_date, 1, 6 ) as d,
          sum( case when device_type = 'D' then visit_count else 0 end ) as dv,
          sum( case when device_type = 'M' then visit_count else 0 end ) as mv,
          sum( visit_count ) as v
        from zdbs_fiori_usage
        where (lv_where)
        group by visit_date
        order by d
        into corresponding fields of table @lt_usage.
      else. " group by date
        select visit_date as d,
          sum( case when device_type = 'D' then visit_count else 0 end ) as dv,
          sum( case when device_type = 'M' then visit_count else 0 end ) as mv,
          sum( visit_count ) as v
        from zdbs_fiori_usage
        where (lv_where)
        group by visit_date
        order by d
        into corresponding fields of table @lt_usage.
      endif.
    elseif lv_query = 'ByApp'. " query by app
      select fiori_app_id as h, min( fiori_app_title ) as a,
       sum( case when device_type = 'D' then visit_count else 0 end ) as dv,
       sum( case when device_type = 'M' then visit_count else 0 end ) as mv,
       sum( visit_count ) as v
     from zdbs_fiori_usage
     where (lv_where)
     group by fiori_app_id
     order by v descending
    into corresponding fields of table @lt_usage.
    elseif lv_query = 'ByUser'. " query by user
      select user_id as u,
       sum( case when device_type = 'D' then visit_count else 0 end ) as dv,
       sum( case when device_type = 'M' then visit_count else 0 end ) as mv,
       sum( visit_count ) as v,
       max( visit_date ) as d
     from zdbs_fiori_usage
     where (lv_where)
     group by user_id
     order by v descending
     into corresponding fields of table @lt_usage.
    elseif lv_query = 'ByDevice'. " query by device type
      select device_type as t,
        sum( visit_count ) as v
      from zdbs_fiori_usage
      where (lv_where)
      group by device_type
      order by v descending
      into corresponding fields of table @lt_usage.
    endif.

    " format lines
    loop at lt_usage assigning <fs_usage>.
      if <fs_usage>-d is not initial.
        concatenate <fs_usage>-d+4(2) '/' <fs_usage>-d+6(2) into <fs_usage>-ds.
        condense <fs_usage>-ds.
      endif.
      " construct key
      if lv_query = 'ByApp'.
        concatenate 'a-'  <fs_usage>-h into  <fs_usage>-k.
      elseif lv_query = 'ByUser'.
        concatenate 'u-'  <fs_usage>-u into  <fs_usage>-k.
      elseif lv_query = 'ByDate'.
        concatenate 'd-'  <fs_usage>-d into  <fs_usage>-k.
      elseif lv_query = 'ByDevice'.
        concatenate 't-'  <fs_usage>-t into  <fs_usage>-k.
      endif.
      if lv_keys is not initial.
        concatenate lv_keys '|' <fs_usage>-k into <fs_usage>-k.
      endif.
      if <fs_usage>-t = 'M'.
        <fs_usage>-t = 'Mobile'.
      elseif <fs_usage>-t = 'D'.
        <fs_usage>-t = 'Desktop'.
      endif.
    endloop.

    move-corresponding lt_usage to et_entityset.

    " handle inline count
    if io_tech_request_context->has_inlinecount( ) = abap_true.
      es_response_context-inlinecount = lines( et_entityset ).
    else.
      clear es_response_context-inlinecount.
    endif.

    " handle paging
    if is_paging-top > 0.
      call method /iwbep/cl_mgw_data_util=>paging
        exporting
          is_paging = is_paging
        changing
          ct_data   = et_entityset.
    endif.
  endmethod.